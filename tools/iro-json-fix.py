#!/usr/bin/env python3

import re
import sys

def main():
    open_square_bracket = re.compile("^\\s*{")
    close_square_bracket = re.compile("^\\s*\\]")
    open_curly_bracket = re.compile("^\\s*\\[}")
    close_curly_bracket = re.compile("^\\s*}")
    key_start = re.compile("^\\s*\"")
    key_string = re.compile("^\\s*\"[^\"]*\"\\s*:\\s\"[^\"]*\"")

    # There's some differences in how Iro and VSCode treat newlines,
    # specifically, in Iro $ includes the newline, and it explicitly
    # rejects matching against \n because Textmate works line-by-line
    #
    # In VSCode, however, $ matches the end of the line, but before
    # the actual newline chatacter.  Fixible by changing the $ to a
    # newline, with the help of a few thousand slashes
    new_line_command_catch = "(?<!\\\\\\\\)(;|$))"
    new_line_command_fix = "(?<!\\\\\\\\)(;|\\n))"
    new_line_simple_catch = "((?<!\\\\)$)"
    new_line_simple_fix = "((?<!\\\\)\\n)"

    javascript_external_catch = '"include" : "#script__2"'
    javascript_external_fix = '"include" : "source.js"'
    with open(sys.argv[1]) as read:
        with open(sys.argv[2], 'w') as out:
            previous_line = "{\n"
            out.write(previous_line)
            for line in read:
                line = line.replace('"', '\\"').replace('\'', '"')
                if new_line_command_catch in line:
                    line = line.replace(new_line_command_catch, new_line_command_fix)
                elif new_line_command_catch in line:
                    line = line.replace(new_line_simple_catch, new_line_simple_fix)
                elif javascript_external_catch in line:
                    line = line.replace(javascript_external_catch, javascript_external_fix)
                if close_square_bracket.match(previous_line) or close_curly_bracket.match(previous_line) or key_string.match(previous_line):
                    if open_square_bracket.match(line) or open_curly_bracket.match(line) or key_start.match(line):
                        out.write(",\n")
                out.write(line)
                previous_line = line
            out.write("}\n")

if __name__ == "__main__":
    main()