# Soar README

This is a VSCode extension adding language support for the [Soar](https://en.wikipedia.org/wiki/Soar_\(cognitive_architecture\)) [Cognitive Architecture](https://en.wikipedia.org/wiki/Cognitive_architecture).

## Features

Syntax highlighting for Soar/NGS!  Get your syntax highlighting for CSoar and JSoar here!  Buy now and get basic highlighting for SoarUnit.  Call in the next twenty minutes and get highlighting for embedded Javascript ABSOLUTELY FREE.

For non-syntax highlighting IDE supports, please [also install the Soar Language Server extension](https://marketplace.visualstudio.com/items?itemName=soartech.soar-language-support).

### TCL Highlighting

Pure TCL code is better highlighted by a dedicated TCL extension, of which there are a few good options in the VSCode marketplace.  Alternatively, you can configure VSCode to use Soar synatx highlighting on TCL source files.

Through the UI, open a TCL file and run "Change Language Mode", select "Configure File Association for '.tcl'...", and select "Soar".  This will add an override for all .tcl files opened in VSCode.  Internally, VSCode adds the following setting to settings.json:

```json
{
    "files.associations": {
        "*.tcl": "soar"
    }
}
```

This setting can be configured in your global settings.json, in which case all .tcl files will treated as Soar.  The same setting can be added to a workplace's .vscode/settings.json, which only modify the current workplace.  Additionally, if your project mixes Soar-heavy and pure TCL files, the key \*.tcl is a standard file path glob and be configured to match specific paths.  For example `"**/soar-tcl/*.tcl": "soar"` would treat any TCL files inside a "soar-tcl" directory as Soar, leaving any other TCL files as TCL.  See [the VSCode documentation](https://code.visualstudio.com/docs/languages/overview#_can-i-map-additional-file-extensions-to-a-language) for more information.

## Roadmap

See [the trello board](https://trello.com/b/sVYjRuiJ/vscode-soar) for full list of known defects and ideas for future features.

### Known Issues

Syntax highlighting drops a handlful of challenging valid constructs, and is overall more generous than the Soar are.  Due to limitations in VSCode's grammar, syntax highlighting is loose on ordering; for example it will happily highlight `(state <s> ^superstate nil)` in a RHS.  TCL highlighting is simplistic, and heavily tuned to the sort of TCL code that's commonly found in NGS.

### Possible Future

* Soar code snippets.
* Grammar improvements to differentuate LHS and RHS in a production, with more correct illegal marking of 'wrong side' code.

## Release Notes

### 0.1.0

Fixed several issues that have come up now that the new Soar Language Server is displacing the old Eclipse Soar IDE, in particular some corner Soar cases and highlight TCL loop flow control.

### 0.0.8

Fix repository links (oops).

### 0.0.7

Hightlight modern NGS constants and add several illegal character checks.

### 0.0.6

Minor bug fix: widen the range of highlighted TCL proc names beyond simple alphanumerics.

### 0.0.5

Added primitive highlighting of TCL proc and if blocks.  Error highlight unexpected sp tokens in production bodies.  Comments should more everywhereable.

### 0.0.4

Improve WME attribute highlighting, don't break on more complex TCL code (recursive curly brackets), and a few minus fixes.

### 0.0.3

SoarUnit support!  One of the few things that's super trivial in Textmate style grammars!  Better support for TCL string highlighting, in particular curly bracket strings.  Still operates under the assumption TCL strings contain either plain text without any weirdness or has Soar code, and highlights accordingly.

### 0.0.2

TCL and more complete Soar grammar!  Hopefully the last major update TBH.

### 0.0.1

Initial release.  Syntax highlighting mostly functional.
