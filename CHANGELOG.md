# Change Log

## 0.1.0

- Add more known NGS constants.
- Add support for nested WMEs.
- Add instructions for treating TCL files as Soar.
- Add very brute force support for TCL loop flow control.
- Highlight outer scope TCL variables, i.e. ($::hello)
- Fix issue with nested conjunctions.
- Bump version number to correspond to actual level of stability.

## 0.0.8

- Fix repository link (oops).

## 0.0.7

- Highlight modern NGS constants like \*YES\* (though you *should* be using $NGS_YES).
- Add illegal highlight checks for a whole bunch of illegal matches, such as TCL stuff in a traditional Soar body, and lots of various things at the root level.

## 0.0.6

- Widen the range of highlighted TCL proc names beyond simple alphanumerics.

## 0.0.5

- Add primative highlighting for TCL proc and if/elseif/else blocks.  Highlighting is not nearly as good as a halfway decent dedicated TCL grammar, but is more tuned to Soar and should work reasonably well, and give you production highlighting if a Soar source file needs a quick if or macro definition.
- Highlight unexpected sp tokens in production bodies, a frequent source of error in my personal work.
- Various misc highlighting fixes, in particular comments should be more universally supported.

## 0.0.4

- Add highlighting for complex attributes.
- Support recursive TCL curly bracket strings (which makes more traditional TCL highlight Good Enough For Now).
- Fix the minus RHS function not hightlighting.  Minus symbols are an endless source of irritation.

## 0.0.3

- Add SoarUnit support.  Whee
- Highlight embedded Javascript code.
- Highlight WMEs contained inside TCL strings.

## 0.0.2

- Finished hopefully all standard Soar grammar, plus a reasonable take on what TCL constructs can be found in NGS Soar code.

## 0.0.1

- Initial release.  Basic syntax highlighting for core Soar productions, TCL support remains ongoing.